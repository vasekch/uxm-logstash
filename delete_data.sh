#!/usr/bin/env bash

# set IP if it didn't came from deploy.sh script
if [[ -z $IP ]]
then
    IP=127.0.0.1
fi

P="n"

if [[ -z $UXM_DEPLOYMENT_IS_CONFIRMED ]]
then
read -p "This will permanetly DELETE uxm-data indices in Elasticsearch! (see delete_data.sh) IP $IP Are you sure? " -n 1 -r
    echo
    P=$REPLY
else
    P="y"
fi

if [[ $P =~ ^[Yy]$ ]]
then
    # do dangerous stuff

    # echo
    # echo "Deleting uxm-raw aliased indices"
    # curl -XDELETE "$IP:9200/uxm-raw?pretty"
    # exitcode="$?"
    # echo "exitcode: $exitcode"

    echo
    echo "Deleting uxm-data aliased indices"
    curl -XDELETE "$IP:9200/uxm-data?pretty"
    exitcode="$?"
    echo "exitcode: $exitcode"

    # echo
    # echo "Deleting uxm-data aliased indices"
    # curl -XDELETE "$IP:9200/uxm-data/sakuli-frame?pretty"
    # exitcode="$?"
    # echo "exitcode: $exitcode"

fi

