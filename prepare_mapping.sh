#!/usr/bin/env bash

# set IP if it didn't came from deploy.sh script
if [[ -z $IP ]]
then
    # IP=172.16.2.21
    # IP=192.168.8.89
    IP=127.0.0.1
fi


echo
echo "Loading index template uxm to ES on $IP"

curl -XPUT "$IP:9200/_template/uxm-raw?pretty" -d '{
    "template": "uxm-raw-*",
    "settings": {
        "number_of_shards": 5,
        "number_of_replicas": 0,
        "index.refresh_interval": "5s"
    },
    "aliases" : {
        "uxm-raw" : {}
    },
    "mappings": {
        "_default_": {
            "dynamic_templates" : [ {
                "string_fields" : {
                    "match" : "*",
                    "match_mapping_type" : "string",
                    "mapping" : {
                        "type" : "string",
                        "index" : "not_analyzed"
                    }
                }
            } ],
            "_all": {
                "enabled" : false
            },
            "properties": {
                "@timestamp": {
                    "type": "date",
                    "format": "dateOptionalTime"
                },
                "tags": {
                    "type": "string",
                    "index": "analyzed"
                },
                "log_timestamp": {
                    "type": "date",
                    "format": "dateOptionalTime"
                },
                "log_message": {
                    "type": "string",
                    "index": "analyzed",
                    "fields": {
                        "raw": {
                            "type": "string",
                            "index": "not_analyzed",
                            "ignore_above" : 256
                        }
                    }
                },
                "suite_start_timestamp": {
                        "type": "date",
                        "format": "dateOptionalTime"
                },
                "suite_end_timestamp": {
                        "type": "date",
                        "format": "dateOptionalTime"
                },
                "suite_frame_start": {
                        "type": "date",
                        "format": "dateOptionalTime"
                },
                "uxm_processed": {
                    "type": "boolean"
                }
            }
        }
    }
}'
exitcode="$?"
echo "exitcode: $exitcode"

curl -XPUT "$IP:9200/_template/uxm-data?pretty" -d '{
    "template": "uxm-data-*",
    "settings": {
        "number_of_shards": 5,
        "number_of_replicas": 0,
        "index.refresh_interval": "5s"
    },
    "aliases" : {
        "uxm-data" : {}
    },
    "mappings": {
        "_default_": {
            "dynamic_templates" : [ {
                "string_fields" : {
                    "match" : "*",
                    "match_mapping_type" : "string",
                    "mapping" : {
                        "type" : "string",
                        "index" : "not_analyzed"
                    }
                }
            } ],
            "_all": {
                "enabled" : false
            },
            "properties": {
                "@timestamp": {
                    "type": "date",
                    "format": "dateOptionalTime"
                },
                "tags": {
                    "type": "string",
                    "index": "analyzed"
                },
                "uxm_processed": {
                    "type": "boolean"
                }

            }
        },
        "sakuli-case": {
            "properties": {
                "suite_start_timestamp": {
                        "type": "date",
                        "format": "dateOptionalTime"
                },
                "case_start_timestamp": {
                        "type": "date",
                        "format": "dateOptionalTime"
                },
                "case_end_timestamp": {
                        "type": "date",
                        "format": "dateOptionalTime"
                },
                "case_frame_start": {
                        "type": "date",
                        "format": "dateOptionalTime"
                }
            }
        },
        "sakuli-frame": {
            "properties": {
                "case_start_timestamp": {
                        "type": "date",
                        "format": "dateOptionalTime"
                },
                "case_frame_start": {
                        "type": "date",
                        "format": "dateOptionalTime"
                },
                "frame_maintenance": {
                    "type": "boolean"
                }
            }
        }
    }
}'
exitcode="$?"
echo "exitcode: $exitcode"

# echo
# echo "Creating today's index"

# curl -XPUT "$IP:9200/uxm-`date +%Y.%m.%d`/?pretty" -d '{
# }'
# exitcode="$?"
# echo "exitcode: $exitcode"

