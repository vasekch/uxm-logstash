require "logstash/filters/base"
require "logstash/namespace"
require "logstash/util/fieldreference"

# TODO change doc this comes from elasticearch.rb
# Search elasticsearch for a previous log event and copy some fields from it
# into the current event.  Below is a complete example of how this filter might
# be used.  Whenever logstash receives an "end" event, it uses this elasticsearch
# filter to find the matching "start" event based on some operation identifier.
# Then it copies the @timestamp field from the "start" event into a new field on
# the "end" event.  Finally, using a combination of the "date" filter and the
# "ruby" filter, we calculate the time duration in hours between the two events.
#
#       if [type] == "end" {
#          elasticsearch {
#             hosts => ["es-server"]
#             query => "type:start AND operation:%{[opid]}"
#             fields => ["@timestamp", "started"]
#          }
#
#          date {
#             match => ["[started]", "ISO8601"]
#             target => "[started]"
#          }
#
#          ruby {
#             code => "event['duration_hrs'] = (event['@timestamp'] - event['started']) / 3600 rescue nil"
#          }
#       }
#
class LogStash::Filters::ElasticsearchGet < LogStash::Filters::Base
  config_name "elasticsearch_get"
  milestone 1

  # List of elasticsearch hosts to use for querying.
  config :hosts, :validate => :array

  # Elasticsearch index
  config :es_index, :validate => :string

  # Elasticsearch type
  config :es_type, :validate => :string

  # Elasticsearch document
  config :es_id, :validate => :string

  # Hash of fields to copy from old event (found via elasticsearch) into new event
  config :fields, :validate => :hash, :default => {}

  public
  def register
    require "elasticsearch"

    @logger.info("New ElasticSearch GET filter", :hosts => @hosts)
    @client = Elasticsearch::Client.new hosts: @hosts
  end # def register

  public
  def filter(event)
    return unless filter?(event)

    begin
      index_str = event.sprintf(@es_index)
      type_str = event.sprintf(@es_type)
      id_str = event.sprintf(@es_id)

      result = @client.get index: index_str, type: type_str, id: id_str

      @fields.each do |old, new|
        event[new] = result['_source'][old]
      end

      filter_matched(event)
    rescue => e
      # TODO supress stdout spamming only when 404 not found
      # @logger.warn("Failed to query elasticsearch GET for previous event",
                   # :index => index_str, :type => type_str, :id => id_str, :event => event, :error => e)
    end
  end # def filter
end # class LogStash::Filters::Elasticsearch
