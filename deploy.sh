#!/usr/bin/env bash
#set -x

if [[ "$#" -ne 1 ]]
then
    echo "1 argument required, $# provided"
    exit 1
fi

# parse args - logstash type - one of event | suite | frame
if ! [[ $1 =~ ^(event|suite|frame)$ ]]
then
    echo "Argument must be one of event|suite|frame"
    exit 1
fi

ARG=$1

read -p "This will deploy scripts to system directories and restart. Are you sure? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
# if [[ 1 ]]
then
    # do dangerous stuff

    # copy modules input, filters and output
    echo "Logstash CONTRIB"
    echo "deploying filter grep"
    cp ./filters/grep.rb /opt/logstash/lib/logstash/filters/

    echo "UXM modules"
    echo "deploying filter elasticsearch_get"
    cp ./filters/elasticsearch_get.rb /opt/logstash/lib/logstash/filters/
    echo "deploying filter normalize_result_state"
    cp ./filters/normalize_result_state.rb /opt/logstash/lib/logstash/filters/
    # echo "deploying filter split_debug"
    # cp ./filters/split_debug.rb /opt/logstash/lib/logstash/filters/
    echo "deploying input elasticsearch_infinite"
    cp ./inputs/elasticsearch_infinite.rb /opt/logstash/lib/logstash/inputs/

    # copy conf file to /etc
    echo "deploying uxm-sakuli-$ARG.conf"
    cp ./uxm-sakuli-$ARG.conf /etc/logstash/conf.d/

    if [[ $ARG = "event" ]]
    then
        echo "configuring event"
        sed -i 's/localhost/es1.uxmlab.rtdn.cz/' /etc/logstash/conf.d/uxm-sakuli-event.conf
        sed -i 's/logstash-forwarder/ls2_uxmlab_rtdn_cz/' /etc/logstash/conf.d/uxm-sakuli-event.conf
    fi

    if [[ $ARG = "suite" ]]
    then
        echo "configuring suite"
        sed -i 's/localhost/es1.uxmlab.rtdn.cz/' /etc/logstash/conf.d/uxm-sakuli-suite.conf
    fi

    if [[ $ARG = "frame" ]]

    then
        echo "configuring frame"
        sed -i 's/localhost/es1.uxmlab.rtdn.cz/' /etc/logstash/conf.d/uxm-sakuli-frame.conf
    fi

    # restart logstash service
    /etc/init.d/logstash restart
fi
