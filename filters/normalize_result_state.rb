require "logstash/filters/base"
require "logstash/namespace"
require "logstash/util/fieldreference"

# This filter normalizes result states ("WARNING_IN_SUITE", "ERRORS") to it's
# basic value ("WARNING", "ERROR"). It is used when parsing sakuli results
#
class LogStash::Filters::NormalizeResultState < LogStash::Filters::Base
  config_name "normalize_result_state"
  milestone 1

  # List of elasticsearch hosts to use for querying.
  config :fields, :validate => :array

  public
    def register
    # Nothing
    end # def register

  public
  def filter(event)
    return unless filter?(event)

    begin
      # normalize result state
      for field in @fields
        if ( event[field] =~ /^ERROR/ )
          event[field] = "ERROR"
        end
        if ( event[field] =~ /^CRITICAL/ )
          event[field] = "CRITICAL"
        end
        if ( event[field] =~ /^WARNING/ )
          event[field] = "WARNING"
        end
      end

      filter_matched(event)
    rescue => e
      @logger.warn("Exception in filter NormalizeResultState",
                   :event => event, :error => e)
    end
  end # def filter
end # class LogStash::Filters::NormalizeResultState
