# encoding: utf-8
require "logstash/inputs/base"
require "logstash/namespace"
require "logstash/util/socket_peer"

# modification of original elasticsearch input
# keeps running even when empty result (waiting for new data to appear)
#
# original:
# Read from an Elasticsearch cluster, based on search query results.
# This is useful for replaying test logs, reindexing, etc.
#
# Example:
#
#     input {
#       # Read all documents from Elasticsearch matching the given query
#       elasticsearch {
#         host => "localhost"
#         query => "ERROR"
#       }
#     }
#
# This would create an Elasticsearch query with the following format:
#
#     http://localhost:9200/logstash-*/_search?q=ERROR&scroll=1m&size=1000
#
# * TODO(sissel): Option to keep the index, type, and doc id so we can do reindexing?
class LogStash::Inputs::ElasticsearchInfinite < LogStash::Inputs::Base
  config_name "elasticsearch_infinite"
  milestone 1

  default :codec, "json"

  # The IP address or hostname of your Elasticsearch server.
  config :host, :validate => :string, :required => true

  # The HTTP port of your Elasticsearch server's REST interface.
  config :port, :validate => :number, :default => 9200

  # The index or alias to search.
  config :index, :validate => :string, :default => "logstash-*"

  # The query to be executed.
  config :query, :validate => :string, :default => "*"

  # This allows you to set the maximum number of hits returned per query.
  config :size, :validate => :number, :default => 1000

  # This parameter controls the infinite loop sleep time, it applies per
  # round trip (i.e. between the previous request, to the next).
  config :delay, :validate => :number, :default => "60"



  public
  def register
    require "ftw"
    @agent = FTW::Agent.new
    params = {
      "q" => @query,
      "size" => "#{@size}",
    }

    @url = "http://#{@host}:#{@port}/#{@index}/_search?#{encode(params)}"
  end # def register

  private
  def encode(hash)
    return hash.collect do |key, value|
      CGI.escape(key) + "=" + CGI.escape(value)
    end.join("&")
  end # def encode

  public
  def run(output_queue)

    while true
      # Execute the search request
      response = @agent.get!(@url)
      json = ""
      response.read_body { |c| json << c }
      result = JSON.parse(json)

      break if result.nil?

      if result["error"]
        @logger.warn(result["error"], :request => @url)
        # dont break, wait for data
        sleep(@delay)
        next
      end

      hits = result["hits"]["hits"]

      # uxm hack don't end whend empty
      # break if hits.empty?

      hits.each do |hit|
        event = hit["_source"]

        # Hack to make codecs work
        @codec.decode(event.to_json) do |event|
          decorate(event)
          output_queue << event
        end
      end

      @logger.debug? && @logger.debug("Sleeping", :delay => @delay)
      sleep(@delay)
    end # while true

  rescue LogStash::ShutdownSignal
    # Do nothing, let us quit.
  end # def run
end # class LogStash::Inputs::Elasticsearch
